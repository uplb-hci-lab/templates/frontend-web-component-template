/**
 * @module
 * @description Webpack configuration for production
 *
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const merge = require('webpack-merge');
const ManifestPlugin = require('webpack-manifest-plugin');
const AppManifestWebpackPlugin = require('app-manifest-webpack-plugin');
const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new ManifestPlugin(),
    new AppManifestWebpackPlugin({
      logo: './src/assets/logo/logo.png',
      output: '/icons-[hash:8]/',
      inject: true,
      config: {
        appName: 'Webpack App',
        appDescription: '',
        developerName: '',
        developerURL: '',
        background: '#fff',
        theme_color: '#fff',
        display: 'standalone',
        orientation: 'portrait',
        start_url: '/',
        version: '1.0',
        logging: true,
        icons: {
          android: true,
          appleIcon: true,
          appleStartup: true,
          faviocns: true,
          firefox: true,
          windows: true,
          yandex: true
        }
      }
    })
  ]
});
