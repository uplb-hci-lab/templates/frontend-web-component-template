declare module 'is-invalid-path';

// merge into WindowEventMap
declare global {
  interface WindowEventMap {
    OnRewards: CustomEvent
  }
}

interface AddDocumentPagePrompt {
  pageTitle: string,
  path: string | null,
  pageIndex: string,
  weight: number
}

interface AddFilePrompt {
  path: string,
  description: string
}

interface JSDocTemplateData {
  id: string,
  longname: string,
  name: string,
  kind: string,
  thisvalue: any,
  license: string | undefined,
  isExported: boolean | undefined,
  description: string | undefined,
  memberof: string | undefined,
  scope: string | undefined
  ignore: boolean | undefined
  params: [
    {
      type: {
        names: [string]
      },
      name: string
      description: string | undefined
    }
  ] | undefined,
  returns: [
    {
      type: {
        names: [string]
      },
      description: string
    }
  ] | undefined,
  meta: {
    lineno: number,
    filename: string,
    path: string
  },
  order: number,
  functions: [JSDocTemplateData]
  variables: [JSDocTemplateData]
}

interface JSDocTemplateStructure {
  [key: string]: JSDocTemplateData
}
