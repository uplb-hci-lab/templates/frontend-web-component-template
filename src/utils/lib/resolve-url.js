/**
 * @module
 * @description Resolves URL
 *
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @type {boolean} */
let workingURL;
/** @type {Document} */
let urlDoc;
/** @type {HTMLBaseElement} */
let urlBase;
/** @type {HTMLAnchorElement} */
let anchor;

/**
 *
 * @param {string} path
 * @param {string} base
 */
export const resolveUrl = (path, base) => {
  if (workingURL === undefined) {
    workingURL = false;
    try {
      const u = new window.URL('b', 'http://a');
      u.pathname = 'c%20d';
      workingURL = (u.href === 'http://a/c%20d');
      workingURL = workingURL && (new window.URL('http://www.google.com/?foo bar').href === 'http://www.google.com/?foo%20bar');
    } catch (error) {
      console.error(error);
    }
  }

  if (workingURL) {
    return new window.URL(path, base);
  }

  if (!urlDoc) {
    urlDoc = document.implementation.createHTMLDocument('url');
    urlBase = urlDoc.createElement('base');
    urlDoc.head.appendChild(urlBase);
    anchor = urlDoc.createElement('a');
  }
  urlBase.href = base;
  anchor.href = path.replace(/ /g, '%20');
  return anchor;
};
