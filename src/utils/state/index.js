const functions = new Map();
const reverseFns = new Map();

export const state = new Map();

/**
 *
 * @param {string} key
 * @param {function} fn
 */
export const subscribe = (key, fn) => {
  if (reverseFns.get(fn)) {
    unsubscribe(fn);
  }

  reverseFns.set(fn, key);

  if (!functions.has(key)) functions.set(key, new Map());

  const map = functions.get(key);

  if (!map.has(fn)) map.set(fn, fn);

  map.get(fn)(state.get(key));
};

/**
 *
 * @param {function} fn
 */
export const unsubscribe = fn => {
  const key = reverseFns.get(fn);
  const map = functions.get(key);
  if (map.has(fn)) map.delete(fn);
  reverseFns.delete(fn);
};

/**
 *
 * @param {string} key
 * @param {*} value
 */
export const updateState = (key, value) => {
  state.set(key, value);
  if (functions.has(key)) {
    for (const fn of functions.get(key).values()) {
      fn(value);
    }
  }
};
