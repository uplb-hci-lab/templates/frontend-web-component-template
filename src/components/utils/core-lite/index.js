/**
 * @module
 * @description Router Lite
 *
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  LitElement, customElement
} from 'lit-element';

import { template } from './template';
import { updateState } from '../../../utils/state';

import '../location-lite';
import '../query-lite';
import '../router-lite';
import '../router-data-lite';

@customElement('core-lite')
class CoreLite extends LitElement {
  render () {
    return template.bind(this)();
  }

  connectedCallback () {
    super.connectedCallback();
    this._initialized = false;
  }

  /**
   *
   * @param {*} _changedProperties
   */
  updated (_changedProperties) {
    super.updated(_changedProperties);

    if (!this._initialized) {
      this.routerLite = (/** @type {import('../router-lite').RouterLite} */(this.renderRoot.querySelector('router-lite')));
      this.queryLite = (/** @type {import('../query-lite').QueryLite} */(this.renderRoot.querySelector('query-lite')));
      this._initialized = true;
    }
  }

  /**
   *
   * @param {CustomEvent} event
   */
  _locationLitePathChange (event) {
    const { detail: path } = event;
    if (path) {
      updateState('path', path);
      if (this.routerLite) {
        this.routerLite.path = path;
      }
    }
  }

  /**
   *
   * @param {CustomEvent} event
   */
  _locationLiteHashChange (event) {
    const { detail: hash } = event;
    updateState('hash', hash);
  }

  /**
   *
   * @param {CustomEvent} event
   */
  _locationLiteQueryChange (event) {
    const { detail: query } = event;
    updateState('query', query);
    if (this.queryLite) {
      this.queryLite.query = query;
    }
  }

  /**
   *
   * @param {CustomEvent} event
   */
  _queryLiteQueryObjectChange (event) {
    const { detail: queryObject } = event;
    updateState('queryObject', queryObject);
  }

  /**
   *
   * @param {CustomEvent} event
   */
  _routerLiteRouteParamObjectChange (event) {
    const { detail } = event;
    updateState('routeParamObject', detail);
    this.dispatchEvent(new window.CustomEvent('route-param-object-change', { detail }));
  }

  /**
   *
   * @param {CustomEvent} event
   */
  _routerLiteCurrentRouteChange (event) {
    const { detail } = event;
    if (detail) {
      updateState('currentRoute', detail);
      this.dispatchEvent(new window.CustomEvent('current-route-change', { detail }));
    }
  }
}

export { CoreLite };
