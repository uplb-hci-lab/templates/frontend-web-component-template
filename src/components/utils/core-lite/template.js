import { html } from 'lit-element';

/**
 * @this {import('../core-lite').CoreLite}
 */
export function template () {
  return html`
    <location-lite
        @path-change="${this._locationLitePathChange}"
        @hash-change="${this._locationLiteHashChange}"
        @query-change="${this._locationLiteQueryChange}"></location-lite>
    <query-lite
        @query-object-change="${this._queryLiteQueryObjectChange}"></query-lite>

    <!-- CHANGE ROUTES HERE -->
    <router-lite
        @route-param-object-change="${this._routerLiteRouteParamObjectChange}"
        @current-route-change="${this._routerLiteCurrentRouteChange}">

      <router-data-lite route="/"></router-data-lite>
      <router-data-lite route="/a/:id"></router-data-lite>

    </router-lite>
  `;
}
