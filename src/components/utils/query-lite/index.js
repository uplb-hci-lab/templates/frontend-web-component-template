/**
 * @module
 * @description Query Lite element
 *
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  LitElement, customElement
} from 'lit-element';

@customElement('query-lite')
class QueryLite extends LitElement {
  /** @param {string} value */
  set query (value) {
    const oldValue = this.query;
    this._queryChanged(value);
    this.requestUpdate('urlSpaceRegex', oldValue);
  }

  /** @param {object} value */
  set queryObject (value) {
    const oldValue = this.queryObject;
    this._queryObjectChanged(value);
    this.requestUpdate('urlSpaceRegex', oldValue);
  }

  static get properties () {
    return {
      query: { type: String },
      queryObject: { type: Object }
    };
  }

  connectedCallback () {
    super.connectedCallback();
    this.queryObject = {};
    this._dontReact = false;
    this._dontReactQuery = false;
  }

  /**
   *
   * @param {string} query
   */
  _queryChanged (query) {
    if (this._dontReactQuery) return;
    this._dontReact = true;
    this.queryObject = this.decodeParams(query);
    Promise.resolve().then(() =>
      this.dispatchEvent(new window.CustomEvent('query-object-change', { detail: this.queryObject })));

    this._dontReact = false;
  }

  /**
   *
   * @param {Object<string, *>} queryObject
   */
  _queryObjectChanged (queryObject) {
    if (this._dontReact) return;
    this._dontReactQuery = true;
    this.query = this.encodeParams(queryObject)
      .replace(/%3F/g, '?')
      .replace(/%2F/g, '/')
      .replace(/'/g, '%27');

    Promise.resolve().then(() =>
      this.dispatchEvent(new window.CustomEvent('query-change', { detail: this.query })));

    this._dontReactQuery = false;
  }

  /**
   *
   * @param {Object<string, *>} params
   */
  encodeParams (params) {
    const encodedParams = [];
    for (const key in params) {
      const value = params[key];
      if (value === '') {
        encodedParams.push(window.encodeURIComponent(key));
      } else if (value) {
        encodedParams.push(`${window.encodeURIComponent(key)}=${window.encodeURIComponent(value.toString())}`);
      }
    }
    return encodedParams.join('&');
  }

  /**
   *
   * @param {string} paramString
   */
  decodeParams (paramString) {
    /**
     * @type {Object<string, *>}
     */
    const params = {};

    paramString = (paramString || '').replace(/\+/g, '%20');
    const paramList = paramString.split('&');
    for (const paramItem of paramList) {
      const [key, value] = paramItem.split('=');
      if (key) {
        params[window.decodeURIComponent(key)] = window.decodeURIComponent(value);
      }
    }
    return params;
  }
}

export { QueryLite };
