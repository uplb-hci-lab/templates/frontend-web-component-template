/**
 * @module
 * @description Router Lite
 *
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  LitElement, customElement
} from 'lit-element';

import pathToRegexp from '../../../utils/lib/path-to-regexp';

@customElement('router-lite')
class RouterLite extends LitElement {
  static get properties () {
    return {
      fallbackRoute: {
        type: String,
        attribute: 'fallback-route'
      },
      currentRoute: {
        type: String
      },
      path: {
        type: String
      }
    };
  }

  /**
   * @param {string} newValue
   */
  set path (newValue) {
    const oldValue = this.path;
    if (this._routeInitialized) {
      this._pathChanged(newValue);
    }
    this.requestUpdate('path', oldValue);
  }

  constructor () {
    super();

    this._boundChildrenChanged = this._childrenChanged.bind(this);
    /** @type {Array<*>} */
    this.routes = [];
    this._routeInitialized = false;
  }

  connectedCallback () {
    super.connectedCallback();

    if (!this.fallbackRoute) this.fallbackRoute = 'no-page';

    if (this.children) {
      for (const child of Array.from(this.children)) {
        this._getRoute(child);
      }
    }

    this._observer = new window.MutationObserver(this._boundChildrenChanged);
    this._observer.observe(this, { childList: true });

    this._routeInitialized = true;
    if (this.path) this._pathChanged(this.path);
  }

  disconnectedCallback () {
    if (super.disconnectedCallback && typeof super.disconnectedCallback === 'function') {
      super.disconnectedCallback();
    }

    if (this._observer) this._observer.disconnect();
  }

  /**
   *
   * @param {Array<*>} changes
   */
  _childrenChanged (changes) {
    for (const change of changes) {
      if (change.type === 'childList' && change.addedNodes) {
        for (const child of Array.from(change.addedNodes)) {
          this._getRoute(child);
        }
      }
    }
  }

  /**
   *
   * @param {*} child
   */
  _getRoute (child) {
    if (child.nodeName.toLowerCase() === 'router-data-lite') {
      if (child.route && typeof child.route === 'string' && child.route.trim()) {
        this.routes.push(child.route);
      } else if (child.hasAttribute('route') && typeof child.getAttribute('route') === 'string' && child.getAttribute('route').trim()) {
        this.routes.push(child.getAttribute('route'));
      } else {
        console.warn('There\'s no route information found in route-data', child);
      }
    }
  }

  /**
   *
   * @param {*} path
   */
  _pathChanged (path = '/') {
    let exec = null;
    let re = null;
    /**
     * @type {Array<*>}
     */
    let keys = [];

    for (const route of this.routes) {
      keys = [];
      re = pathToRegexp(route, keys);
      exec = re.exec(path);

      if (exec) return this._routeMatched(route, exec, keys);
    }

    return this._routeMatched(this.fallbackRoute, [], []);
  }

  /**
   *
   * @param {*} route
   * @param {*} exec
   * @param {Array<*>} keys
   */
  _routeMatched (route, exec, keys) {
    /**
     * @type {Object<string, *>}
     */
    const params = {};
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const { name } = key;
      params[name] = exec[i + 1] || null;
    }

    this.routeParamObject = params;
    this.currentRoute = route;

    Promise.resolve().then(() => {
      this.dispatchEvent(new window.CustomEvent('route-param-object-change', { detail: this.routeParamObject }));
      this.dispatchEvent(new window.CustomEvent('current-route-change', { detail: this.currentRoute }));
    });
  }
}

export { RouterLite };
