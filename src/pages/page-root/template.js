import { html } from 'lit-element';

/**
 * @this {import('./index').Page}
 */
export function template () {
  return html`
    Hello World
    <a href="/a/b"> Here</a>
  `;
}
