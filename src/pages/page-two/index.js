/**
 * @module
 * @description Page Root
 *
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  LitElement, customElement, property
} from 'lit-element';

import { template } from './template';
import { subscribe, unsubscribe } from '../../utils/state';

@customElement('page-two')
class Page extends LitElement {
  @property({ type: String })
  b = ''

  render () {
    return template.bind(this)();
  }

  constructor () {
    super();
    this._boundGetRouteParamObject = this._getRouteParamObject.bind(this);
  }

  connectedCallback () {
    super.connectedCallback();
    subscribe('routeParamObject', this._boundGetRouteParamObject);
  }

  disconnectedCallback () {
    if (typeof super.disconnectedCallback === 'function') super.disconnectedCallback();
    unsubscribe(this._boundGetRouteParamObject);
  }

  /**
   *
   * @param {*} routeParamObject
   */
  _getRouteParamObject (routeParamObject) {
    const { id } = routeParamObject;
    this.b = id;
  }
}

export { Page };
