import { html } from 'lit-element';

/**
 * @this {import('./index').Page}
 */
export function template () {
  return html`
    Hello World Two ${this.b}
    <a href="/"> Root</a>
  `;
}
