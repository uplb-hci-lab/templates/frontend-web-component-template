/**
 * @module
 * @description This is the source file for the bundle
 *
 * @license
 * Copyright 2020, UPLB HCI Lab Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import './above-the-fold.sass';
import './components/utils/core-lite';
import { subscribe } from './utils/state';

/**
 *
 * @param {string} node
 */
const changePage = (node) => {
  const el = document.createElement(node);
  const main = document.querySelector('main');
  while (main?.lastElementChild) {
    main.removeChild(main.lastElementChild);
  }
  console.log(main?.appendChild(el));
};

/**
 *
 * @param {string} value
 */
const routeChanger = async (value) => {
  if (value) {
    switch (value) {
      case '/':
        await import(/* webpackPreferch: true, webpackChunkName: "page-root" */'./pages/page-root');
        changePage('page-root');
        break;
      case '/a/:id':
        await import(/* webpackPreferch: true, webpackChunkName: "page-two" */'./pages/page-two');
        changePage('page-two');
        break;
      case 'no-page':
      default:
        await import(/* webpackPreferch: true, webpackChunkName: "page-root" */'./pages/page-root');
        changePage('page-root');
        break;
    }
  }
};

subscribe('currentRoute', routeChanger);
